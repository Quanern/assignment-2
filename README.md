# Assignment 2

This is the second assignment for the upskill Java course at Noroff. The assignment is meant to teach us this:
- How to do user input.
- How to manipulate a file.
    - Getting metadata of a file.
    - Searching in the file.
- How to write in a file (logging).

This program is separated into 3 different parts: getting information about files, searching through a file and logging.
## Installation guide
1. Clone or download the repository.
2. Open as project in IntelliJ.
3. Build the project and run Main.

## User input
The program takes user input to determine what action the user wants to take. If the input given is outside of the
scope of the program, the user will be prompted to enter a new input.

## Getting information about files
The user can get the name of all the files in the project's resources folder, or all the files of one type, i.e. .txt.
The user can also get the name, size in kB and number of lines of a predetermined file.

## Searching in the file
Given a predetermined file called *Dracula.txt*, the user can search how many occurrences of a specific word there is 
in the file. The program lets the user do a strict and a loose search. In the strict search, only exact occurrences of
the word counts, while in loose any word that contains the search word is counted. Example is if the user searches for
the term *dracula*, in loose search the word *dracula's* will be counted, while in strict it won't.

## Logging
Any time the user calls a function to get information or search in file, a log.txt will be updated with the timestamp,
what action and runtime of the function.