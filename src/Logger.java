import java.io.File;
import java.io.IOException;
import java.io.FileWriter;

class Logger{
    private long startTime;
    private long endTime;
    private long duration;
    private String date = java.time.LocalDateTime.now().toString();
    private final File logFile = new File("./logs/log.txt");

    void createDirAndLog(){
        //Creates the directory for logs if it doesn't exist.
            logFile.getParentFile().mkdir();
    }


    void draculaSearchCountLogger(String searchword, int wordCount, String strictness, long runtime){
        //Logs when the user searched through Dracula.txt, how long it took and what word they searched for.
        try {
            FileWriter writer = new FileWriter(logFile, true);
            writer.write(date + " The user searched for the term \"" + searchword +
                    "\" and the term was found " + wordCount + " times. It was a " + strictness +" search and took "
                    + runtime + "ms to execute\n");
            writer.close();
        } catch(IOException e) {
            System.out.println("Error writing the log" + e);
        }
    }

    void draculaGetInformationLogger(long runtime) {
        //Logs when and how long it took to look up information about Dracula.txt
        try {
            FileWriter writer = new FileWriter(logFile, true);
            writer.write(date + " The user looked for information about Dracula.txt. It took "+ runtime +"ms " +
                    "to execute\n");
            writer.close();
        } catch(IOException e) {
            System.out.println("Error writing the log" + e);
        }
    }

    void getAllFilesLogger(long runtime) {
        //Logs when and how long it took to get all the files in the resources folder.
        try {
            FileWriter writer = new FileWriter(logFile, true);
            writer.write(date + " The user looked for all files in resources. It took "+runtime+ "ms to execute\n");
            writer.close();
        } catch(IOException e) {
            System.out.println("Error writing the log" + e);
        }
    }

    void getAllFilesByTypeLogger(String type, long runtime) {
        //Logs when and how long it took to get files depending on type in the resources folder.
        try {
            FileWriter writer = new FileWriter(logFile, true);
            writer.write(date + " The user looked for all "+ type + "-files in resources. It took "+
                    runtime+ "ms to execute\n");
            writer.close();
        } catch(IOException e) {
            System.out.println("Error writing the log" + e);
        }
    }

    void fileNotFoundLogger(){
        //Logs when and that a fileNotFoundException occurred.
        try {
            FileWriter writer = new FileWriter(logFile, true);
            writer.write(date + " A file not found exception occurred\n");
            writer.close();
        } catch(IOException e) {
            System.out.println("Error writing the log" + e);
        }
    }

    void IOExceptionLogger(){
        //Logs when and that an IOException occurred.
        try {
            FileWriter writer = new FileWriter(logFile, true);
            writer.write(date + " An IO exception occurred\n");
            writer.close();
        } catch(IOException e) {
            System.out.println("Error writing the log" + e);
        }
    }

    void setDuration(){
        this.duration = (getEndTime()-getStartTime())/1000000; //converts to milliseconds(ms)
    }

    long getDuration(){
        return duration;
    }

    private long getStartTime() {
        return startTime;
    }

    void setStartTime() {
        this.startTime = System.nanoTime();
    }

    private long getEndTime() {
        return endTime;
    }

    void setEndTime() {
        this.endTime = System.nanoTime();
    }

}