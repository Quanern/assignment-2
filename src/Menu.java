import java.sql.SQLOutput;
import java.util.*;

class Menu {
    private FileHandlers fileHandlers = new FileHandlers();

    private void mainMenuText() {
        //"\033 changes the color to [93m which is yellow. [0m is the default color.
        System.out.println("\033[93mEnter 1 for files in the resources folder.");
        System.out.println("Enter 2 for Dracula.txt.");
        System.out.println("Enter 0 to exit the program.\033[0m");
    }




    private void filesInResourceFolderText() {
        //"\033 changes the color to [93m which is yellow. [0m is the default color.
        System.out.println("\033[93mEnter 1 for a list of all files in the resources folder.");
        System.out.println("Enter 2 for a list of all jfif files in the resources folder.");
        System.out.println("Enter 3 for a list of all jpeg files in the resources folder.");
        System.out.println("Enter 4 for a list of all jpg files in the resources folder.");
        System.out.println("Enter 5 for a list of all png files in the resources folder.");
        System.out.println("Enter 6 for a list of all txt files in the resources folder.");
        System.out.println("Enter 0 to return to the main menu.\033[0m");
    }


    private void filesInResourceFolder(int listOptions) {
        //Switch with options to decide what kind of files you want a list of.
            switch (listOptions) {
                case 1:
                    fileHandlers.getListOfAllFiles();
                    break;
                case 2:
                    fileHandlers.getListOfFilesOfType(".jfif");
                    break;
                case 3:
                    fileHandlers.getListOfFilesOfType(".jpeg");
                    break;
                case 4:
                    fileHandlers.getListOfFilesOfType(".jpg");
                    break;
                case 5:
                    fileHandlers.getListOfFilesOfType(".png");
                    break;
                case 6:
                    fileHandlers.getListOfFilesOfType(".txt");
                    break;
            }

    }

    private void forDraculatxtText() {
        //"\033 changes the color to [93m which is yellow. [0m is the default color.
        System.out.println("\033[93mEnter 1 for information about Dracula.");
        System.out.println("Enter 2 to search for a word in Dracula and see how many times it occurs.");
        System.out.println("Enter 3 to search for a word in Dracula and see how many times words that" +
                " contains it occurs.");
        System.out.println("Enter 0 to return to the main menu.\033[0m");
    }

    private void forDraculatxt(int draculaOptions, Scanner scanner) {
        //Switch with options with what to do with dracula.txt
        switch (draculaOptions) {
            case 1:
                fileHandlers.getDraculaInformation();
                break;
            case 2:
                fileHandlers.draculaSearch(scanner, true);
                break;
            case 3:
                fileHandlers.draculaSearch(scanner, false);
                break;

        }
    }

    private void inputNotAnInteger(Scanner scanner){
        //Method ensures that the program won't crash when anything other than an integer is entered.
        if (!scanner.hasNextInt()) {
            while (!scanner.hasNextInt()) {
                inputNotAnIntegerText();
                scanner.next(); //jump to next line to not be stuck on the same input indefinitely.
            }
        }
    }

    private int inputNotInScope(Scanner scanner, int max){
        //Method keeps the user in the current menu whenever an integer outside the scope of the menu is entered.
        int number = 0;
        if(scanner.hasNextInt()){
            int temp = scanner.nextInt();
            while(temp>max || temp < 0){ //if temp is outside of the scope, it will enter this while-loop
                inputNotInScopeText();
                if(!scanner.hasNextInt()){ //if the next input is not an integer, it enters the inputNotAnInteger -
                    inputNotAnInteger(scanner);// function and loops until a new integer is entered.
                }
                temp = scanner.nextInt();//sets temp as the new input after the not an integer check.
            }
            number = temp;
        }

        return number;
    }

    private void inputNotInScopeText() {
        //"\033 changes the color to [31m which is red. [0m is the default color.
        //This is the error message when the user enters a number outside of the choices.
        System.out.println("\033[31mInput not within the scope\033[0m");
    }

    private void inputNotAnIntegerText() {
        //"\033 changes the color to [31m which is red. [0m is the default color.
        //This is the error message when the user enters anything but a number.
        System.out.println("\033[31mYour choice must be an integer within the scope.\033[0m");
    }

    void mainMenu() {
        //Method calls the rest of the menu-methods to create a menu.
        Scanner scanner = new Scanner(System.in); //initiates the Scanner class
        boolean quit = false; //set a boolean quit as false.
        //"\033 changes the color to [31m which is cyan. [0m is the default color.
        System.out.println("\033[36mHello, and welcome to this program.\033[0m");
        while (!quit) { //As long as quit is false, the program will loop
            boolean innerLoop = false; //Set to false to start the loop.
            mainMenuText();
            inputNotAnInteger(scanner); //Sanitizes the input to be a number
            int options = inputNotInScope(scanner, 2); //If number is outside the scope(2), it will enter a loop
            switch (options) {
                case 1:
                    //This case is for getting the names of the different files in the resources folder
                    while(!innerLoop){ //Enters a loop since innerLoop is never true
                        filesInResourceFolderText();
                        inputNotAnInteger(scanner);
                        int listOptions = inputNotInScope(scanner, 6); //Sets the max scope to (6).
                        filesInResourceFolder(listOptions);
                        //Switch with the different methods for getting information about files in the resources folder
                        if(listOptions == 0) //Breaks out of the loop if the user enters 0.
                            innerLoop=true;
                    }
                    break;
                case 2:
                    while(!innerLoop) {
                        //This case is for searching in or getting information about Dracula.txt
                        forDraculatxtText();
                        inputNotAnInteger(scanner);
                        int draculaOptions = inputNotInScope(scanner, 3); //Sets the max scope to (3).
                        forDraculatxt(draculaOptions, scanner);
                        if(draculaOptions == 0) //Breaks out of the loop if the user enters 0.
                            innerLoop=true;
                    }
                    break;
                case 0:
                    quit = true; //Changes the boolean quit to true and exits the program.
                    break;
            }
        }
        System.out.println("Thank you for using this program. Have a nice day");
        scanner.close();
    }

}
