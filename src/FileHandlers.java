import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

class FileHandlers {
    private final File draculaPath = new File("./src/resources/Dracula.txt");
    private final File resourcesPath = new File("./src./resources");
    private Logger logger = new Logger();

    void getListOfAllFiles() {
        logger.setStartTime(); //Sets the time when function began.
        String[] contents = resourcesPath.list(); //Array of all the files in the resources folder.
        System.out.println("List of files in the resources folder:");
        for (String fileName: contents) { //For-each-loop to print out the contents array.
            System.out.println(fileName);
        }
        logger.setEndTime(); //Sets the time when function stopped.
        logger.setDuration(); //Sets the duration of the function.
        logger.getAllFilesLogger(logger.getDuration()); //Logs the time and what function the user used.
    }

    void getListOfFilesOfType(String fileType) {
        logger.setStartTime(); //Sets the time when the function began
        FilenameFilter textFilefilter = (File dir, String name) -> { //FilenameFilter to filter the different files.
                String lowercaseName = name.toLowerCase(); //String with the names of documents in lowercase.
                return lowercaseName.endsWith(fileType); //Returns the files that ends in the filtered type.
        };
        String[] filesList = resourcesPath.list(textFilefilter); //Array of all the files with the type in the folder.
        System.out.println("List of all " + fileType + " files in the resources folder");
        for (String fileName : filesList) { //For-each-loop to print out the contents array.
            System.out.println(fileName);
        }
        logger.setEndTime(); //Sets the time when function stopped.
        logger.setDuration(); //Sets the duration of the function.
        logger.getAllFilesByTypeLogger(fileType, logger.getDuration()); //Logs the time and what function the user used.

    }

    private void getDraculaFileName() {
        //Method to get the name of Dracula.txt
        if (draculaPath.exists()) {
            System.out.println("Name of the file: " + draculaPath.getName());
        }
    }

    private void getDraculaSize(){
        //Method to get the size of Dracula.txt in kB
        if (draculaPath.exists()) {
            System.out.println("Size of the file: " + draculaPath.length() / 1024 + "kB");
        }
    }

    private void getDraculaLines() {
        //Method to get how many lines there are in Dracula.txt
        try {
            BufferedReader reader = new BufferedReader(new FileReader(draculaPath));
            int lines = 0; //How many lines the method has read.
            while (reader.readLine() != null) { //As long as the there is a new line, the method will keep reading.
                lines++; //For every line it reads, add 1 to lines.
            }
            reader.close(); //Close the reader when it is done.
            System.out.println("Amount of lines in the file: " + lines);
        } catch(FileNotFoundException e) { //FileNotFoundException in case method can't find the document.
            logger.fileNotFoundLogger();
            System.out.println("File not found error: " + e);
        } catch (IOException e){
            logger.IOExceptionLogger();
            System.out.println("An error occured: "+e);
        }
    }

    void getDraculaInformation(){
        //Puts all three methods that gets information about Dracula.txt into one.
        logger.setStartTime(); //Sets the start time of the function.
        getDraculaFileName(); //Method that gets the name of Dracula.txt
        getDraculaSize(); //Method that gets the size of Dracula.txt in kB
        getDraculaLines(); //Method that gets the amount of lines in Dracula.txt
        logger.setEndTime(); //Sets the time for when the function stopped.
        logger.setDuration(); //Sets the duration of the function
        logger.draculaGetInformationLogger(logger.getDuration()); //Logs what function the user used and the time.
    }

    void draculaSearch(Scanner scanner, boolean strict) {
        /*Method to search through Dracula.txt. It has a strict and a loose search. They are both case insensitive.
        A strict search searches for the exact word in the file, a loose search searches for any occurrences of
        the word in the document. Strict won't return "Dracula's" as an occurrence, but loose will.*/
        System.out.println("Please enter the search word:");
        String searchword = scanner.next(); //The word the user wants to search for
        int wordCount = 0; //How many times the word has occurred in the document.
        logger.setStartTime(); //Sets the time for when the function started.
        try {
            BufferedReader br = new BufferedReader(new FileReader("./src/resources/Dracula.txt"));
            String readline;
            while((readline = br.readLine()) != null){ //The function will keep reading as long as there is a new line.
                StringTokenizer st = new StringTokenizer(readline, " ");
                //Splits all lines into tokens. Every token is a word that is separated by a space.
                int size = st.countTokens(); //The amount of words in Dracula.txt.
                for (int i = 0; i < size; i++) { //Iterates through every token.
                    String word = st.nextToken().toLowerCase();  //Turns every token into a String in lowercase.
                    if(strict){ //If the user wants a strict search.
                        if(word.equals(searchword.toLowerCase())){
                            //Looks for words that equals the search word in lowercase.
                            wordCount++; //Adds up all the occurrences of the search word.
                        }
                    }
                    else { //If the user wants a loose search.
                        if(word.contains(searchword.toLowerCase())){
                            //Looks for words that contains the search word in lowercase.
                            wordCount++; //Adds up all the occurrences of the search word.
                        }
                    }
                }
            }
            String strictness = strict ? "strict" : "contains in the word"; //In-line if the search is strict or not.
            System.out.println("There is " + wordCount + " occurrences of the word "
                    + searchword.toLowerCase() + " in the file.");
            logger.setEndTime(); //Sets the time when the function finished.
            logger.setDuration(); //Sets the duration of the function.
            logger.draculaSearchCountLogger(searchword, wordCount, strictness, logger.getDuration());
            //Logs what kind of function the user used and the time.
        } catch(FileNotFoundException e) {
            logger.fileNotFoundLogger();
            System.out.println("File not found error: " + e);
        } catch (IOException e){
            logger.IOExceptionLogger();
            System.out.println("An error occured: "+e);
        }
    }
}